---
title: Story
type: post
date: 2020-11-01
---

After seeing the fitness industry in Toronto, and working with coaches from various disciplines, I came to the realization that being a Personal Trainer just wasn’t enough for me. 

When I entered the industry my dream was to help people live happier, healthier, longer lives but what I found was a disconnect between training at the gym and influencing people’s lifestyle. I want to do more than just teach people how to move, I want to help them achieve goals and connect with fitness in the same way I have.

{{<instagram CFwg-NAh1V2 hidecaption >}}