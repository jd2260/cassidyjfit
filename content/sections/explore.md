---
title: "Explore"
date: 2018
type: "main"
---

**Ready to get started** or looking for more information? If you want to get to know more about me and my fitness philosophy check out my blog below!

{{<buttonGroup ref="getstarted"  text="Get Started" ref2="blog" text2="Blog">}}