---
title: "About"
date: 2020
type: "main"
---

**Hi! I’m Cassidy,** and I am a Personal Trainer/Wellness Coach as well as a professional Actor/Singer/Dancer. Coming from a small town in British Columbia, Canada I started my journey in fitness during my time in NYC  studying at The American Musical and Dramatic Academy (AMDA). After graduating from a two year conservatory program, I went on to perform in cabarets and even an off-broadway production. 

Through performing I fell in love with dance, and through dance I fell in love with fitness. Fitness became a constant in my life, not only did I love the way it had transformed my body, but my more active lifestyle helped improve my mental health. With this realization, 

I made the decision to leave NYC to pursue a career in the health & wellness field. Moving back to Canada, I started my work as a Personal Trainer while pursuing theatre, film, and modelling on the side. Although still working in the industry, my dream is to help people live happier, healthier, longer lives. 

Whatever your goals, whatever your needs, let’s find the right *fit* together!

{{<image src="images/pic02.jpg" >}}
