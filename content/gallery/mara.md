---
title: "Mara"
type: "gallery"
imgs: ["01.jpg:landscape", "02.jpg:portrait", "03.jpg:portrait", "04.jpg:landscape"]
---

"I started working with Cassidy a few years ago with the intent to just rehab after a knee injury and move on. Little did I know, working with her would completely change the way I saw, spoke and thought about my body. 
Within months my mindset had shifted from *just get the rehab done* to the goal of getting stronger through continuous and consistent hard work."
