---
title: "Yasmin Al-Samarrai"
type: "gallery"
imgs: ["05.jpg:landscape", "06.jpg:portrait", "07.jpg:portrait"]
---

Cassidy is **very** creative; she will use your dining chair, couch, pillows, and make you do things you never thought you would do in your living room. We met 2 times a week, and she provided me with 3 other workouts that I can do without her - all of which she held me accountable for. What I love about her too is that she takes time to really explain the exercise to you, in profound detail, narrating what you should and shouldn’t be feeling, all the while focusing on your strengths and improving your weaknesses.
