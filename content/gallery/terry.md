---
title: "Terry L."
type: "gallery"
imgs: ["08.jpg:portrait", "09.jpg:portrait", "10.jpg:landscape"]
---

"The Devil, Satan, demon - are some of the things I call Cassidy during our training sessions (and some which I can't write down). She's got eyes in the back of her head so if you're thinking of slacking off for part of that set, she's on you faster than a raccoon going through leftovers. If you need someone to distract you during a particular hard-set, she can do that while helping you keep count." 

"She's a **pint-sized drill-sergeant** that makes me sweat buckets (or tears, I can't tell) but I felt the strongest and fittest when I was training with her!"
